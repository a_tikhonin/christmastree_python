from classes.christmas_tree import ChristmasTree


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def main():
    while True:
        print("Введите количество этажей ёлки (min = 5): ")
        tree_level = input()
        if is_int(tree_level):
            if int(tree_level) >= 5:
                break

    print("\nВведите путь для сохранения ёлки (default = output.txt): ")
    output_path = input()

    print()

    my_tree = ChristmasTree(tree_level, output_path)
    my_tree.print()
    my_tree.save_to_txt()


main()



