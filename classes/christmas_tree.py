class ChristmasTree:
    def __init__(self, level, output):
        # Уровни ёлки (-2 верхних, которые не меняются)
        self.level = int(level) - 2

        # Путь до файла, в котором будет сохранена ёлка
        self.output = output

        # Запуск метода генерации ёлки
        self._create_tree()

    def _create_tree(self):
        self.tree = []
        self.tree.append('W')
        self.tree.append('***')
        i = 0
        while i < int(self.level):
            level_str = '****' + ('*' * i * 2)
            if (i % 2) == 0:
                self.tree.append("@" + level_str)
            else:
                self.tree.append(level_str + "@")
            i += 1
        last_level_len = len(self.tree[i + 1])
        self.tree.append("TTTTT")
        self.tree.append("TTTTT")

        i = 0
        while i < len(self.tree):
            spaces_count = int((last_level_len - len(self.tree[i]))/2)
            self.tree[i] = (' ' * spaces_count) + self.tree[i]
            i += 1

    def print(self):
        for i in self.tree:
            print(i)

    def save_to_txt(self):
        if len(self.output) == 0:
            self.output += 'output.txt'
        if not self.output.endswith('.txt'):
            self.output += '.txt'
        try:
            f = open(self.output, 'w')
            for i in self.tree:
                f.write(i + '\n')
            f.close()
            print('\n' + 'Файл успешно сохранён!')
        except IOError:
            print('\n' + 'Ошибка сохранения файла')
